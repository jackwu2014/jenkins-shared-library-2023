package com.luffy.devops

/**
 *
 * @param fileId 全局配置文件的ID
 * @param model 执行命令：package|deploy
 * @return
 */
def init(String fileId,String model){
    this.fileId = fileId
    this.model = model
    return this
}

def model() {
	configFileProvider([configFile(fileId: "${this.fileId}", targetLocation: "settings.xml")]){
		//获取全局工具Maven的配置
		def  maven = tool name: 'maven', type: 'maven'
		
		// 执行 Maven 命令构建项目，并且设置 Maven 配置为刚刚创建的 Settings.xml 文件
		sh "${maven}/bin/mvn -T 1C clean ${this.model} -Dmaven.test.skip=true --settings settings.xml"
	}
}
