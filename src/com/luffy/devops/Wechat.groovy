package com.luffy.devops

import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput

def markDown(text, credentialsId,  Boolean verbose=false) {
    data = [
            'msgtype': 'markdown',
            'markdown': [
                    'content': "${text}"
            ],
    ]
    this.sendMessage(data,credentialsId, verbose)
}

def sendMessage(data, credentialsId, Boolean verbose=false, codes="100:399") {
    def reqBody = new JsonOutput().toJson(data)
    withCredentials([usernamePassword(credentialsId: credentialsId, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        def response = httpRequest(
                httpMode:'POST', url: "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=${PASSWORD}",
                requestBody:reqBody,
                validResponseCodes: codes,
                contentType: "APPLICATION_JSON",
                quiet: !verbose
        )
        def jsonSlurper = new JsonSlurperClassic()
        def json = jsonSlurper.parseText(response.content)
        echo "json response: ${json}"
        return json
    }
}