package org.foo

import groovy.json.JsonSlurper

def jsonSlurper = new JsonSlurper()

def config = jsonSlurper.parse(new File('json.json'))

println(config.metadata.name)
