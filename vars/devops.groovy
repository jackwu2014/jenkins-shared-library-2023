import com.luffy.devops.*

static def hello(String content) {
    return new Hello().init(content)
}

/**
 *
 * @param repo, 172.21.51.143:5000/demo/myblog/xxx/
 * @param tag, v1.0
 * @param dockerfile
 * @param credentialsId
 * @param context
 */
static def docker(String repo, String tag, String credentialsId, String appName, String fileId, String dockerfile="Dockerfile", String context=".") {
    return new Docker().init(repo, tag, credentialsId, appName, fileId, dockerfile, context)
}

static def maven(String fileId, String model="package") {
    return new Maven().init(fileId, model)
}

/**
 * sonarqube scanner
 * @param projectVersion
 * @param waitScan
 * @return
 */
static def scan(String projectVersion="", Boolean waitScan = true) {
    return new Sonar().init(projectVersion, waitScan)
}

/**
 * notificationSuccess
 * @param project
 * @param receiver
 * @param credentialsId
 * @param title
 * @return
 */
static def notificationSuccess(String project, String receiver="dingTalk", String credentialsId="dingTalk", String title=""){
    new Notification().init(project, receiver, credentialsId, title).notification("success")
}

/**
 * notificationFailed
 * @param project
 * @param receiver
 * @param credentialsId
 * @param title
 * @return
 */
static def notificationFailed(String project, String receiver="dingTalk", String credentialsId="dingTalk", String title=""){
    new Notification().init(project, receiver, credentialsId, title).notification("failure")
}

/**
 *
 * @param resourcePath
 * @param watch
 * @param workloadFilePath
 * @return
 */
static def deploy(String resourcePath, Boolean watch = true, String workloadFilePath){
    return new Deploy().init(resourcePath, watch, workloadFilePath)
}

/**
 *
 * @param comp
 * @return
 */
static def robotTest(String comp=""){
    new Robot().acceptanceTest(comp)
}